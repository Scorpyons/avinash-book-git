# Pages

Para carregarmos os componentes que vimos na etapa anterior, é necessário criar a página. A página criada deverá ser do mesmo tipo que o componente criado. Não podemos ter uma página de produtos "Products", carregando um componente do tipo (Schema) artigo (Article).

Nos exemplos anteriores, vimos o componente do tipo "ProductCategory", que foi vinculado a uma keyword criada dentro de "productRoot". Portanto, esse componente só poderá ser carregado em uma página que esteja preparada para receber as informações correspondentes (ProductCategory).

São dentro das páginas também que podemos inserir carrossel de imagens, que são componentes do tipo Hero. Vale lembrar que as páginas são criadas na área 070 [Brand Site-Name].

Abaixo, uma imagem com a lista das páginas criadas do site.

![Alternative text](/images/pages.jpg "Optional title")

<br />

Abaixo, temos a imagem do detalhe da página que estamos usando como exemplo. Você pode observar que ele é do tipo (Schema) "ProductCategory01".

![Alternative text](/images/product-category-page.jpg "Optional title")

Após a página ser criada, ela já pode ser publicada juntamente com os componentes e keywords, e assim, podemos ver o resultado da categoria "Ades Frutas" no site. Vale lembrar que todas as publicações são feitas na área 070 [Brand Site-Name].

![Alternative text](/images/ades-frutas.jpg "Optional title")
