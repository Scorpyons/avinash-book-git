# Components

Um componente de conteúdo pode representar várias coisas: Produto, categoria, Teaser, imagem, videos, etc.

Nos próximos artigos, você poderá ver exemplos de tipos de componentes e como e onde eles são aplicados.
