# O que é Tridion?

Tridion é a ferramenta CMS que usamos para gerenciar os conteúdos dos sites independentemente do Look and Feel.

![Alternative text](/images/tridion.jpg "Optional title")

figura1: Imagem da tela inicial do Tridion.

![Alternative text](/images/tridion-lista.jpg "Optional title")

figura2: Imagem da tela de listagem dos sites no Tridion.
