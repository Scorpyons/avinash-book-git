# Keywords

Quando vamos categorizar algo como por exemplo um produto ou um artigo, criamos uma keyword. Com a keyword criada, podemos criar e vincular componentes, como um componente do tipo ProductCategory, que veremos na próxima etapa.

<br />

![Alternative text](/images/keyword.jpg "Optional title")

<br />

No exemplo acima, temos algumas keywords criadas dentro de "productRoot". Com essas keywords criadas, é possível vincular os componentes do tipo ProductCategory com elas, e assim, é possível criar as categorias desses produtos.

Vale lembrar que tudo que for preciso ser criado, deve ser criado em 050 [Brand Name]. Na sessão Components, você pode continuar a leitura para entender como o componente é vinculado a Keyword que você viu na imagem acima.
