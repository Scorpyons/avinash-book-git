# Terminologias

Algumas terminologias que usamos ao trabalhar com o Tridion:

##CMS

* Software que é usado para gerenciar o conteúdo de um website;
* Permite o gerenciamento de conteúdo independente da aparência do site e sensação;
* O CMS que usamos é o SDL Tridion;

##Content component

Um componente de conteúdo é um tipo de item de conteúdo que gerenciamos no CMS. Componentes de conteúdo podem aparecer em várias páginas em todo o site.

Tipos de componentes comuns utilizados nos sites:

* Products
* Categories
* Heroes
* Teasers
* Articles
* Promo Images

##Keywords

As Keywords são usadas para vincular categorias ou templates das páginas.

##Page

Corresponde a uma página ou URL no site.

##Template (Schema)

* Define quais itens de conteúdo será exibido em uma página;
* Está associado a um layout que dita como o conteúdo será exibido;

##Publication

Publicação é a ação que fazemos quando terminamos a criação/edição de componentes e páginas. Através da publicação que conseguimos atualizar o site.
