# ProductCategory

Os componentes podem aparecer em várias páginas em todo o site. Abaixo, a imagem mostra a estrutura de onde o componente tipo ProductCategory (Ades Frutas) está organizado.

![Alternative text](/images/component-category.jpg "Optional title")

<br />

Na etapa anterior, você pode ver e entender onde as keywords são criadas e organizadas. Recapitulando, criamos uma keyword quando vamos categorizar algo como por exemplo um produto ou categoria. Na imagem abaixo, você pode verificar o detalhe de um componente, que nesse exemplo é um componente do tipo ProductCategory.

![Alternative text](/images/product-category.jpg "Optional title")

<br />

Nesse exemplo, você pode verificar que o componente é do tipo (Schema) ProductCategory, e também que vinculamos esse componente com a Keyword Ades Frutas.

Para esse componente ser publicado no site, ainda temos uma etapa de criação da página que vai carregar esse componente. Consulte a sessão "Pages" para entender como é esse processo.
