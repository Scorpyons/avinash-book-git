# Entendendo os Componentes e Páginas

Abaixo, a tabela mostra exemplos de páginas e o tipo de componentes que podemos criar para cada uma delas.

| Pages        | Components           |
| ------------- |:-------------:|
| Product Pages (Landing, Category, Detail)      | Images, Heroes, Teasers, Categories, Products, Promo Images, Videos |
| Article Pages (Landing, Category, Detail)      | Images, Heroes, Teasers, Categories, Articles, Promo Images, Videos      |
| Recipe Pages (Search, Search Results, Detail) | Images, Teaser-Recipe-Items, Promo Images      |
| About Us, Help Center pages (Contact Us, Sign Up, FAQ/Virtual Agent) Store Locator | Images, Teasers, Promo Images      |
