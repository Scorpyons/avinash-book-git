# Article Component

Nesse exemplo, você poderá entender o que é e como é aplicado um componente tipo artigo. Abaixo, uma imagem de onde um componente está organizado no site de Ades

![Alternative text](/images/article-tridion.jpg "Optional title")


O componente tipo artigo tem uma área onde podemos inserir HTML, e normalmente é usado quando é necessário criar uma área fora do padrão dos templates, pois temos mais liberdade.

![Alternative text](../images/article-component.jpg "Optional title")


Na imagem acima, você pode observar os detalhes do componente que tem o tipo (schema) Article. Nesse componente, associamos a keyword "Curiosidades" como PrimaryCategory. Se você quer entender mais sobre Keywords, consulte o artigo 2.4.1.

Abaixo, a imagem do artigo aplicado no site.

![Alternative text](/images/article.jpg "Optional title")
