# Estrutura e Organização

Para cada site da marca, temos duas publicações:

* 050 [Brand Name]
* 070 [Brand Site-Name]

## 050 [Brand Name]

Chamamos essa área de "content publication" pois é aqui que os ** conteúdos são criados ** e também onde fazemos a manutenção deles (Articles, images, products, etc).

## 070 [Brand Site-Name]

É nessa área onde ** criamos e damos manutenção às páginas **. Chamamos essa área de "site publication" pois é aqui que todas as páginas, conteúdos e definições de configuração são publicadas.

### Ilustração da organização
![Alternative text](/images/tridion-organization.jpg "Optional title")
